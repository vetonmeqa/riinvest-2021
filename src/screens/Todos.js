import React, { useState, useEffect } from 'react';
import {View, Text, ScrollView, Alert, Platform} from 'react-native';

const Todos = () => {
  const [todos, setTodos] = useState([]);
  const [logading, setLoading] = useState(false);
  const isIOS = Platform.OS === 'ios';

  useEffect(() => { 
    fetchTodos();
  }, []);

  const fetchTodos = () => {
    setLoading(true);
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(todos => setTodos(todos))
      .catch(() => Alert.alert('Please try again!'))
      .finally(() => setLoading(false));
  };

  return (
    <ScrollView>

      {todos.map((todo, index) => (
        <View style={{ height: 50, padding: 10}}>
          <Text style={{ color: isIOS ? 'blue' : 'green' }}>
              {index + 1}. {todo.title} - {todo.completed ? 'Completed' : 'In Progress'}
          </Text>
        </View>
      ))}
  
    </ScrollView>
  );
}

export default Todos;