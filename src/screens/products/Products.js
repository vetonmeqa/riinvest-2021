import React, { useEffect } from 'react';
import {View, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Header from '../../components/Header';
import {getProducts} from './actions';
import ProductsList from './components/ProductsList';

const Products = ({ navigation }) => {
  const dispatch = useDispatch();
  const {products} = useSelector((state) => state.products);

  useEffect(() => {
    dispatch(getProducts());
  }, []);

  const selectProduct = (product) => {
    navigation.navigate("ProductDetail", {productId: product.id});
  };

  return (
    <View style={styles.cointainer}>
      
      <Header />

      <ProductsList
        products={products}
        selectProduct={selectProduct}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  cointainer: {
    flex: 1,
    padding: 15, 
    paddingTop: 50,
    backgroundColor: '#ececec'
  }
});

export default Products;