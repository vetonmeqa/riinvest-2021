import {constants} from "./constants";

export function getProducts() {
  // get products from somewhere

  const products = [{id: 123, name: 'product 1'}, {id: 123213, name: 'product 2'}];

  return {
    type: constants.GET,
    products: products
  };
}

// Idea for implementation of fetch data inside redux
export function getProductsApi() {
    dispatch(loading());

  fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then((products) => dipatch(success(products)))
    .catch(() => dispatch(failure()))

  function loading() {
    return {
      type: constants.GET_LOADING,
    };
  }
  
  function success() {
    return {
      type: constants.GET_SUCCESS,
      products: products
    };
  }

  function failure() {
    return {
      type: constants.GET_FAILURE,
    };
  }
}

export function clear() {
  return {
    type: constants.CLEAR
  }
}
