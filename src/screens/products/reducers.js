import {constants} from "./constants";

const INITIAL_STATE = { products: [] };

export function products(state = INITIAL_STATE, action) {
  switch(action.type) {
    case constants.GET: 
      return {
        products: action.products
      }
    case constants.CLEAR:
      return {
        products: []
      }
    default:
      return state;
  }
}