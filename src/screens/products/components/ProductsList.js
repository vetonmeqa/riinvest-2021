import React from 'react';
import {View, FlatList, Text, StyleSheet} from 'react-native';
import Product from './Product';

const ProductsList = ({ products, selectProduct }) => {
  return (
    <View style={styles.list}>

    {products.length > 0 ? (
      <FlatList 
        data={products}
        renderItem={({item}) => <Product product={item} select={selectProduct} />}
        keyExtractor={item => item.id}
      />)
    : <Text>No products on list!</Text>}

  </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 9, 
    margin: 10, 
  }
});

export default ProductsList;