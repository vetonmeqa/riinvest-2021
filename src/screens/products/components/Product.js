import React, {useContext} from 'react';
import {Text, TouchableOpacity, StyleSheet, View} from 'react-native';
import {themeContext} from '../../../contexts';

const Product = ({ product, select }) => {
  const theme = useContext(themeContext);
  return (
    <TouchableOpacity style={styles.cointainer} onPress={() => select(product)}>
      <View style={{flex: 7}}>
        <Text>
          {product.name} - {theme}
        </Text>
      </View>
      <View style={{flex: 2}}>
        <Text>
          03:30
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({ 
  cointainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'yellow',
    padding: 10,
    margin: 10,
    minHeight: 60
  }
});

export default Product;