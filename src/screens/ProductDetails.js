import React from 'react';
import {View, Text} from 'react-native';

const ProductDetails = ({ route }) => {
  return (
    <View>
      <Text>
        Product Details for product with id {route.params.productId}
      </Text>
    </View>
  );
}

export default ProductDetails;