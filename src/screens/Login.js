import React, { useState } from 'react';
import {Text, TextInput, View, Button, Alert} from 'react-native';

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onLoginClick = () => {
    const user = {
      email: email,
      password: password
    };

    Alert.alert("Your email is: " + user.email + " and your password has " + user.password.length + " characters");
  }

  return (
    <View style={{paddingTop: 50, padding: 30}}>
      
      <Text style={{textAlign: 'center'}}> Login Form </Text>
      
      <View style={{paddingTop: 20}}>
        <Text style={{marginBottom: 5}}>Email:</Text>
        <TextInput
          style={{borderWidth: 1, padding: 5}}
          onChangeText={setEmail}
          value={email} 
          placeholder="Write email here ..."
        /> 
      </View>

      <View style={{paddingTop: 20}}>
        <Text style={{marginBottom: 5}}>Password:</Text>
        <TextInput
          style={{borderWidth: 1, padding: 5}}
          onChangeText={setPassword}
          value={password} 
          secureTextEntry={true}
          placeholder="Write password here ..."
        /> 
      </View>

      <View style={{paddingTop: 20}}>
        <Button
          onPress={onLoginClick}
          title={"Login"}
        />
      </View>

    </View>
  );
}

export default Login;