import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './screens/Home';
import ProductDetails from './screens/ProductDetails';
import Products from './screens/products/Products';
import Todos from './screens/Todos';
import Account from './screens/Account';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={Home} />
      <HomeStack.Screen name="ProductDetail" component={ProductDetails} />
    </HomeStack.Navigator>
  );
}

const ProductStack = createStackNavigator();

function ProductsStackScreen() {
  return (
    <ProductStack.Navigator>
      <ProductStack.Screen name="Products" component={Products} />
      <ProductStack.Screen name="ProductDetail" component={ProductDetails} />
    </ProductStack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

const AppStack = () => {
  return (
    <Tab.Navigator initialRouteName="Home">
      
      <Tab.Screen 
        name="Home" 
        component={HomeStackScreen} 
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="home-outline" color={color} size={size}/>
          ),
        }}
      />

      <Tab.Screen 
        name="Products" 
        component={ProductsStackScreen} 
        options={{
          tabBarLabel: 'Products',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="list-outline" color={color} size={size}/>
          ),
        }}
      />

      <Tab.Screen 
        name="Todos" 
        component={Todos} 
        options={{
          tabBarLabel: 'Todos',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="list-outline" color={color} size={size}/>
          ),
        }}
      />

      <Tab.Screen 
        name="Accout" 
        component={Account} 
        options={{
          tabBarLabel: 'Account',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person-outline" color={color} size={size}/>
          ),
        }}
      />

    </Tab.Navigator>
  );
};

export default AppStack;