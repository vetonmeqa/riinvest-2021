import React from 'react';
import {View, Text} from 'react-native';

const UserItem = ({ user }) => {
  return (
    <View style={{backgroundColor: '#eee', padding: 5}}>
      <Text>
        {user.firstName + " " + user.lastName + " : " + user.age}
      </Text>
    </View>
  );
}

export default UserItem;