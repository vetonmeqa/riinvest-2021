import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

const Header = () => {
  return (
    <View style={styles.header}>
        <Text>Header</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flex: 1, 
    backgroundColor: '#aaa', 
    margin: 10, 
    borderWidth: 2, 
    borderColor: '#eee',
    padding: 10,
    borderRadius: 5
  }
});

export default Header;