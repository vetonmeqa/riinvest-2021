import { NavigationContainer } from '@react-navigation/native';
import React, {useState} from 'react';
import {Provider} from 'react-redux';
import AppStack from './AppStack';
import {themeContext} from './contexts';
import {createStore} from 'redux';
import rootReducer from './reducers';

const store = createStore(rootReducer);

const Main = () => {
  const [theme, setTheme] = useState('light');

  return (
    <Provider store={store}>

      <themeContext.Provider value={theme}>

        <NavigationContainer>

          <AppStack />

        </NavigationContainer>

      </themeContext.Provider>

    </Provider>
  );
}

export default Main;