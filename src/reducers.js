import {combineReducers} from "redux";
import {products} from "./screens/products/reducers";

const rootReducer = combineReducers({
  products,
});

export default rootReducer;